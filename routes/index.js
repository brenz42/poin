var express = require('express');
var router = express.Router();

var etudiants = [];
etudiants.push({id: 1, nom: "CAHIER", prenom: "Francois", hexaCarte: "1337140B"});

const pointages = [];
pointages.push({id: 1, datePrevu: "2021-06-03 08:00:00", dateRealise: "", Etudiant: etudiants[0], etat: "en attente"});
pointages.push({id: 2, datePrevu: "2021-06-04 08:00:00", dateRealise: "", Etudiant: etudiants[0], etat: "à l'heure"});
pointages.push({id: 3, datePrevu: "2021-06-05 08:00:00", dateRealise: "", Etudiant: etudiants[0], etat: "en attente"});
var pointages = [];
pointages.push({id: 1, datePrevu: "2021-06-03 08:00:00", moins: "2021-06-03 00:00:00", plus: "2021-06-03 23:59:59", dateRealise: "", idEtudiant: etudiants[0], etat: "en attente"});
pointages.push({id: 2, datePrevu: "2021-06-04 08:00:00", moins: "2021-06-04 00:00:00", plus: "2021-06-04 23:59:59", dateRealise: "", idEtudiant: etudiants[0], etat: "en attente"});
pointages.push({id: 3, datePrevu: "2021-06-05 08:00:00", moins: "2021-06-05 00:00:00", plus: "2021-06-05 23:59:59", dateRealise: "", idEtudiant: etudiants[0], etat: "en attente"});
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { etudiants: etudiants, pointages: pointages });
});
router.post('/pointage', function (req, res) {
  try {
    //const date = Date.now();
    const date = new Date().toLocaleString("fr-FR", {timeZone: "Europe/Paris"});
    if (etudiants.filter(etudiant => etudiant.hexaCarte.toUpperCase() === req.body.hexaCarte.toUpperCase()).length === 0) {
      console.log("erreur");
      console.log(etudiants[0].hexaCarte);
      console.log(req.body.hexaCarte);
      res.sendStatus(600);
      return;
    }
     pointages.map((p) => {
      if (p.idEtudiant.hexaCarte.toUpperCase() === req.body.hexaCarte.toUpperCase()) {
        if (Date.parse(p.moins) <= Date.parse(date) && Date.parse(p.plus) >= Date.parse(date)) {
          p.etat = "à l'heure";

          let D = new Date(date);
          let d = ("0" + D.getDate()).slice(-2);
          let month = ("0" + (D.getMonth() + 1)).slice(-2);
          let year = D.getFullYear();
          let hours = ("0" + D.getHours()).slice(-2);
          let minutes =  ("0" + D.getMinutes()).slice(-2);
          let seconds =  ("0" + D.getSeconds()).slice(-2);
          p.dateRealise = year + "-" + month + "-" + d + " " + hours + ":" + minutes + ":" + seconds;
        }
      }
    });
    console.log(pointages);

    res.sendStatus(200);
  } catch {
    res.sendStatus(301);
  }

});
module.exports = router;
